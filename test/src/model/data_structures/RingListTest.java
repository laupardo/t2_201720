package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

public class RingListTest {
	
	private RingList<?> lt;
	
	private void setupEscenario1(){
		lt = new RingList(); 
	}
	private void setupEscenario2(){
		lt = new RingList(); 
		lt.addAtEnd("prueba relax con un string bien ramdom");
	}
	private void setupEscenario3(){
		lt = new RingList(); 
		for(int i=0; i<5;i++){
			lt.addAtEnd(i+1); 
		}
	}
	private void setupEscenario4(){
		lt = new RingList(); 
		for(int i=0; i<10;i++){
			lt.addAtEnd(i+1); 
		}
	}

	@Test
	public final void testRingList() {
		setupEscenario1();
		assertNotNull("el atributo first no debe ser null" ,lt.getFirst() );
		assertNotNull("el atributo last no debe ser null" ,lt.getLast() );
		assertNotNull("el atributo iter no debe ser null" ,lt.getIter());
		assertEquals("el atributo n debe ser igual a 0",0, lt.getSize());

	}

	@Test
	public final void testGetSize() {
		setupEscenario3();
		assertEquals("el tamaño de la lista debería de ser 5", 5, lt.getSize());
	
	}

	

	@Test
	public final void testAddAtEnd() {
		setupEscenario1();
		lt.addAtEnd(1);
		lt.addAtEnd(2);
		lt.addAtEnd("hola bb");
		lt.addAtEnd("dark sirius");
	
		assertEquals("el item del nodo debe ser el 1",1, lt.getIter().next());
		assertEquals("el item del nodo debe ser el 2",2, lt.getIter().next());
		assertEquals("el item del nodo debe ser la cadena: hola bb","hola bb", lt.getIter().next());
		assertEquals("el item del nodo debe ser la cadena: dark siruis","dark sirius", lt.getIter().next());

	
	}

	@Test
	public final void testAddAtK() {
		setupEscenario3();
		lt.addAtK(1, "ya van a ser las doce y falta un resto :(");
		lt.addAtK(3, 23);  
		lt.addAtK(4, 24); 
		lt.addAtK(5, 25); 
		lt.addAtK(6, 26); 
		lt.addAtK(2, "hola soy una prueba");
		lt.addAtK(30, 12);
		
		assertEquals("el  nodo debe tener el elemento una cadena muy grande","ya van a ser las doce y falta un resto :(", lt.getIter().next());
		assertEquals("el  nodo debe tener el elemento cadena mas pequeña","hola soy una prueba", lt.getIter().next());
		assertEquals("el  nodo debe tener el elemento 1",1, lt.getIter().next());
		assertEquals("el  nodo debe tener el elemento 23",23, lt.getIter().next());
		assertEquals("el  nodo debe tener el elemento 24",24, lt.getIter().next());
		assertEquals("el  nodo debe tener el elemento 25",25, lt.getIter().next());
		assertEquals("el  nodo debe tener el elemento 26",26, lt.getIter().next());
		assertEquals("el  nodo debe tener el elemento 22",12, lt.getIter().next());
		assertEquals("el nodo debe tener el elemento 3",2, lt.getIter().next());

	}

	@Test
	public final void testGetElement() {
		setupEscenario4();
		assertEquals("el  nodo debe tener el elemento entero 1",1, lt.getElement(1));
		assertEquals("el  nodo debe tener el elemento entero 3",3, lt.getElement(3));
		assertEquals("el  nodo debe tener el elementoentero 6",6, lt.getElement(6));
		assertEquals("el  nodo debe tener el elemento entero 5",5, lt.getElement(5));
		assertEquals("el nodo debe tener el elementoentero 2",2, lt.getElement(2));
		assertEquals("el  nodo debe tener el elemento entero 7",7, lt.getElement(7));
		assertEquals("el  nodo debe tener el elemento entero 9",9, lt.getElement(39));
		assertEquals("el  nodo debe tener el elemento entero 4",4, lt.getElement(14));
		assertEquals("el  nodo debe tener el elemento entero 2",2, lt.getElement(22));


	}

	

	@Test
	public final void testDelete() {
		setupEscenario4();
		assertTrue("debe retornar true " ,lt.delete(3)==true);
		assertTrue("debe retornar true " ,lt.delete(5)==true);
		assertTrue("debe retornar true " ,lt.delete(8)==true);
		assertTrue("debe retornar true " ,lt.delete(7)==true);
		assertTrue("debe retornar true " ,lt.delete(1)==true);
		assertTrue("debe retornar true " ,lt.delete(2)==true);
		assertTrue("debe retornar true " ,lt.delete(6)==true);
	}

	@Test
	public final void testDeleteAtK() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testNext() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testPrevious() {
		fail("Not yet implemented"); // TODO
	}

}
