package controller;

import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {

	private static final String DATA_ROUTES = "data/routes.txt";
	private static final String DATA_TRIPS = "data/trips.txt";
	private static final String DATA_STOP_TIMES = "data/stop_times.txt";
	private static final String DATA_STOPS = "data/stops.txt";
	/**
	 * Reference to the routes and stops manager
	 */
	private static STSManager  manager = new STSManager();
	

	
	public static void loadRoutes() {

		manager.loadRoutes(DATA_ROUTES);
	}
		
	public static void loadTrips() {
		manager.loadTrips(DATA_TRIPS);
	}

	public static void loadStopTimes() {
		manager.loadStopTimes(DATA_STOP_TIMES);
	}
	
	public static void loadStops() {
		manager.loadStops(DATA_STOPS);

	}
	
	public static IList<VORoute> routeAtStop(String stopName) {
		return null;
	}
	
	public static IList<VOStop> stopsRoute(String routeName, String direction) {
		return null;
	}
}
