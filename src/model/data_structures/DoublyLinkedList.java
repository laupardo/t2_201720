package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T>  implements IList, Iterable {

	private Node whereAmI;
	private Node first;     
	private Node last;
	private Iterator<T> iter;
	private int n;
	
	public DoublyLinkedList() 
	{
		first = new Node();
		last = new Node();
		whereAmI=first;
		last.prev=first;
		first.next = last;
		iter= new LiIterator() ;

		n=0;

	}


	public Node getFirst(){
		return first;
	}
	public Node getLast(){
		return last;
	}
	public Iterator getIter(){
		return iter;
	}

	public class Node 
	{
		private T element=null;
		private Node next;
		private Node prev;

	}

	public class LiIterator implements Iterator<T> 
	{
		private Node curr = first;

		public boolean hasNext()
		{
			return curr!=null;
		}
		public T next() 
		{
			whereAmI=curr.next;
			Node temp = curr.next;
			curr = curr.next;

			return  temp.element;
		}

		public void remove() {}

	}


	@Override
	public int getSize() 
	{

		return n;
	}

	/**
	 * Agrega un elemento despues del primer nodo. Si el primer nodo no tiene elemento le asigna el elemento. 
	 * @param item-elemento que se va a agregar
	 */

	public void add(Object item) 
	{

		Node anterior = last.prev;
		Node x = new Node();
		x.element =(T) item;
		x.next = last;
		x.prev = anterior;
		last.prev = x;
		anterior.next = x;
		n++ ;

	}

	@Override
	public void addAtEnd(Object item) {

		Node anterior = last.prev;
		Node x = new Node();
		x.element =(T) item;
		x.next = last;
		x.prev = anterior;
		last.prev = x;
		anterior.next = x;
		n++;
	}

	/**
	 * agrega un elemento en una posicion
	 * @param index indice, se asume que pide el numero del elemento no el indice (empieza en 1 en vez de cero)
	 * @return true si se elimino false de lo contrario
	 */
	public void addAtK(int index, Object item) 
	{
		Node temp = new Node();

		temp.element=(T)item;

		if(index<getSize()&&index!=0){
			int count =0;
			boolean l=false;
			while(iter.hasNext()&&!l){
				if(index==count){

					Node before=whereAmI.prev;
					whereAmI.prev=temp;
					temp.next=whereAmI;
					before.next= temp;
					temp.prev=before;
					iter= new LiIterator() ;
					l=true;
					n++;
				}
				else{
					iter.next();
					count++;
				}

			}
		}

	}

	@Override
	public T getElement(int index) {

		T ans=null;
		if(index<getSize()&&index!=0)
		{
			int count=0;
			boolean l=false;
			while(iter.hasNext()&& !l)
			{
				count ++;
				ans = iter.next();
				if(count==index)
				{
					l=true;
					iter= new LiIterator() ;
				}

			}

		}

		return ans;
	}

	@Override
	public T getCurrentElement() 
	{
		return whereAmI.element;
	}



	@Override
	public boolean delete(Object item) {

		boolean ans =false;

		while(iter.hasNext()&&!ans){
			T it=iter.next();
			if(it.equals(item)){

				Node y= whereAmI.prev;
				Node x =  whereAmI.next;
				y.next= x;
				x.prev= y;
				ans =true;
				iter= new LiIterator() ;
			}

		}
		n--;
		return ans;
	}

	/**
	 * Elimina un elemento en una posicion
	 * @param index indice, se asume que pide el numero del elemento no el indice (empieza en 1 en vez de cero)
	 * @return true si se elimino false de lo contrario
	 */
	public boolean deleteAtK(int index) {
		boolean l=false;
		if(index<getSize()&&index!=0){
			int count =0;
			while(iter.hasNext()&&!l){
				count++;
				T it=iter.next();
				if(index==count){
					iter= new LiIterator() ;
					delete(it);
					n--;
					l=true;
				}
			}
		}
		return l;
	}

	@Override
	public Object next() {

		return whereAmI.next.element;
	}

	@Override
	public Object previous() {

		return whereAmI.prev.element;
	}

	@Override
	public Iterator iterator() {
		return new LiIterator();
	}

}
