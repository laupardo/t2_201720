package model.data_structures;

import java.util.Iterator;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	public int getSize();
	public void add (T item);
	public void addAtEnd(T item);
	public void addAtK(int index,T item );
	public T getElement(int index);
	public T getCurrentElement();
	public boolean delete(T item);
	public boolean deleteAtK(int index);
	public T next();
	public T previous();
	
	
}
