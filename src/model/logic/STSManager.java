package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrips;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {


	DoublyLinkedList<VORoute> routeList = new DoublyLinkedList<VORoute>();
	DoublyLinkedList<VOStopTimes> stopTimeList = new DoublyLinkedList<VOStopTimes>();
	RingList<VOStop> stopList= new RingList<VOStop>();
	RingList<VOTrips> tripList = new RingList<VOTrips>();

	public void loadRoutes(String routesFile) {
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(routesFile));
			String line = br.readLine();
			line = br.readLine();



			while (line!=null)
			{

				String[] input = line.split(",");
				VORoute ruta= new VORoute(Integer.parseInt(input[0]), input[1], input[2], input[3], input[4], Integer.parseInt(input[5]), (input[6]), input[7], input[8]);
				routeList.addAtEnd(ruta);
				line = br.readLine();
			}
			br.close();

		}
		catch(Exception e)
		{

			e.printStackTrace();

		}


	}

	@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		Iterator<VORoute> iter = routeList.iterator();
		RingList<VORoute> outerRouteList = new RingList<VORoute>();
		VORoute one = iter.next();
		//while (iter.hasNext())
		{
			System.out.println("entra al while");
			VORoute temp = iter.next();
			System.out.println(temp.id() +"//"+ one.id());
			System.out.println(routeList.getCurrentElement().id());
			
				outerRouteList.addAtEnd(routeList.getCurrentElement());
				one=temp;
			
		}
		try
		{ System.out.println("entra al try");
			DoublyLinkedList<VOTrips> trips = new DoublyLinkedList<VOTrips>();
			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			String line = br.readLine();
			while (line!=null)
			{
				String[] input = line.split(",");
				VOTrips tempTrip= new VOTrips(Integer.parseInt(input[0]), Integer.parseInt(input[1]), Integer.parseInt(input[2]), input[3], input[4], Integer.parseInt(input[5]), Integer.parseInt(input[6]), Integer.parseInt(input[7]), Integer.parseInt(input[8]), Integer.parseInt(input[9]));
				trips.addAtEnd(tempTrip);
				line = br.readLine();
			}
			br.close();


		}
		catch(Exception e)
		{
        System.out.println("el metodo esta puteado");
		}


	}

	@Override
	public void loadStopTimes(String stopTimesFile) {
		
		try
		{
			System.out.println("entra al try de stopTimes");
			BufferedReader br = new BufferedReader(new FileReader(stopTimesFile));
			System.out.println("lee el archivo cvs de stiopTimes");
			String line = br.readLine();
			line = br.readLine();

			while (line!=null)
			{
				System.out.println(line);
				String[] input = new String[9];

				String[] inputP= line.split(",");
				inputP[5]="0";
				
				if(inputP.length == 9){
					input =inputP;
				}
				else{
					for(int i=0; i<input.length-1;i++){

						
						input[i] = inputP[i];	
						
					}

					input[8]="0.0";
				}
				
				VOStopTimes stopTimes= new VOStopTimes(Integer.parseInt(input[0]), input[1], input[2], Integer.parseInt(input[3]), Integer.parseInt(input[4]), Integer.parseInt(input[5]), input[6], Integer.parseInt(input[7]), Double.parseDouble(input[8]));
				stopTimeList.addAtEnd(stopTimes);
				line = br.readLine();
			}
			br.close();

		}
		catch(Exception e)
		{
			System.out.println("no cargo stopTimes data");
		}


	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String line = br.readLine();
			while (line!=null)
			{
				//split
			}
			br.close();

		}
		catch(Exception e)
		{

		}

	}




	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		return null;
	}

}
