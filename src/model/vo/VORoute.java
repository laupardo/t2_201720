package model.vo;

import model.data_structures.DoublyLinkedList;

/**
 * Representation of a route object
 */
public class VORoute {

	int id;
	
	String agencyId;
	String routeShortName;
	String routeLongName;
	String routeDesc;
	int routeType;
	String routeUrl;
	String routeColor;
	String routeTextColor;
	DoublyLinkedList<VOTrips> trips;
	
	public VORoute(int pId,String pAgencyId,String pRouteShortName,String pRouteLongName,String pRouteDesc,int pRouteType,
	String pRouteUrl,String pRouteColor,String pRouteTextColor )
	{
	
		id=pId;
		agencyId=pAgencyId;
		routeShortName=pRouteShortName;
		routeLongName=pRouteLongName;
		routeDesc=pRouteDesc;
		routeType=pRouteType;
		routeUrl=pRouteUrl;
		routeColor=pRouteColor;
		routeTextColor=pRouteTextColor;
		trips = new DoublyLinkedList<VOTrips>();
	}
	/**
	 * @return id - Route's id number
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return name - route name
	 */
	
	
	public String getAgencyId()
	{
		return agencyId;
	}
	public String getRouteShortName()
	{
		return routeShortName;
	}
	public String getRouteLongName()
	{
		return routeLongName;
		
	}
	public String getRouteDesc()
	{
		return routeDesc;
		
	}
	public int getRouteType()
	{
		return routeType;
		
	}
	public String getRouteUrl()
	{
		return routeUrl;
		
	}
	public String getRouteColor()
	{
		return routeColor;
		
	}
	public String getRouteTextColor()
	{
		return routeTextColor;
		
	}
	public void addTrips(DoublyLinkedList<VOTrips> pTrips)
	{
		trips=pTrips;
	}

}
