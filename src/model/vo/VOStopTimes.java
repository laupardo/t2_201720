package model.vo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VOStopTimes {
	int tripId;
	Date arrivalTime;
	Date departureTime;
	int stopId;
	int stopSequence;
	int stopHeadsign;
	String pickupType;
	int dropOffType;
	double shapeDistTraveled;

	public VOStopTimes(int pTripId,String pArrivalTime,String pDepartureTime, int pStopId, int pStopSequence,int pStopHeadsign, String pPickupType,
			int pDropOffType, double pShapeDistTraveled) throws Exception
	{

		tripId=pTripId;
		DateFormat arrivalTimeFormat=new SimpleDateFormat("hh:mm:ss");
		arrivalTime=arrivalTimeFormat.parse(pArrivalTime);
		DateFormat departureTimeFormat=new SimpleDateFormat("hh:mm:ss");
		departureTime=departureTimeFormat.parse(pDepartureTime);
		stopId=pStopId;
		stopSequence=pStopSequence;
		stopHeadsign=pStopHeadsign;
		pickupType=pPickupType;
		dropOffType=pDropOffType;
		shapeDistTraveled=pShapeDistTraveled;
	}
	
	public int getTripId()
	{
		return tripId;
		
	}
	public Date getArrivalTime()
	{
		return arrivalTime;
		
	}
	public Date getDepartureTime()
	{
		return departureTime;
		
	}
	public int getStopId()
	{
		return stopId;
		
	}
	public int getStopSequence()
	{
		return stopSequence;
		
	}
	public int getStopHeadsign()
	{
		return stopHeadsign;
		
	}
	public String getPickupType()
	{
		return pickupType;
		
	}
	public int getDropOffType()
	{
		return dropOffType;
		
	}
	public double shapeDistTraveled()
	{
		return shapeDistTraveled;
		
	}
}
