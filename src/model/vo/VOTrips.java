package model.vo;

import model.data_structures.DoublyLinkedList;

public class VOTrips {
	int routeId;
	int serviceId;
	int tripId;
	String tripHeadsign;
	String tripShortName;
	int directionId;
	int blockId;
	int shapeId;
	int wheelchairAccessible;
	int bikesAllowed;
	
	public VOTrips(int pRouteId,int pServiceId, int pTripId,String pTripHeadsign,String pTripShortName, int pDirectionId, 
			int pBlockId, int pShapeId, int pWheelchairAccessible, int pBikesAllowed)
	{
		
		 routeId=pRouteId;
		 serviceId=pServiceId;
		 tripId=pTripId;
		 tripHeadsign=pTripHeadsign;
		 tripShortName=pTripShortName;
		 directionId=pDirectionId;
		 blockId=pBlockId;
		 shapeId=pShapeId;
		 wheelchairAccessible=pWheelchairAccessible;
		 bikesAllowed=pBikesAllowed;
	}
	public int getRouteId()
	{
		return routeId;
	}
	public  int getServiceId()
	{
		return serviceId;
	}
	public int getTripId()
	{
		return tripId;
	}
	public String getTripHeadsign()
	{
		return tripHeadsign;
	}
	public String getTripShortName()
	{
		return tripShortName;
	}
	public int getDirectionId()
	{
		return directionId;
	}
	public int getBlockId()
	{
		return blockId;
	}
	public int getShapeId()
	{
		return shapeId;
	}
	public int getWheelchairAccessible()
	{
		return wheelchairAccessible;
	}
	public int getBikesAllowed()
	{
		return bikesAllowed;
	}
}



