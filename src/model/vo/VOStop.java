package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {

	int stopId;
	int stopCode;
	String stopName;
	String stopDesc;
	double stopLat;
	double stopLon;
	String zoneId;
	String stopUrl;
	int locationType;
	String parentStation;
	
	public VOStop(int pStopId,int pStopCode, String pStopName, String pStopDesc, double pStopLat,double pStopLon,
			String pZoneId, String pStopUrl, int pLocationType, String pParentStation)
	{
		stopId=pStopId;
		stopCode=pStopCode;
		stopName=pStopName;
		stopDesc=pStopDesc;
		stopLat=pStopLat;
		stopLon=pStopLon;
		zoneId=pZoneId;
		stopUrl=pStopUrl;
		locationType=pLocationType;
		parentStation=pParentStation;
	}
	
	public int getStopId()
	{
		return stopId;
		
	}
	public int getStopCode()
	{
		return stopCode;
	}
	public String getStopName()
	{
		return stopName;
	}
	public String getStopDesc()
	{
		return stopDesc;
	}
	public double getStopLat()
	{
		return stopLat;
	}
	public double getStopLon()
	{
		return stopLon;
	}
	public String getZoneId()
	{
		return zoneId;
	}
	public String getStopUrl()
	{
		return stopUrl;
	}
	public int getLocationType()
	{
		return locationType;
	}
	public String getParentStation()
	{
		return parentStation;
	}

}
